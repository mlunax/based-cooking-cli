module gitlab.com/mlunax/based-cooking-cli

go 1.16

require (
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/urfave/cli/v2 v2.3.0
)
