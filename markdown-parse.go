package main

import (
	"regexp"
)

type Group struct {
	Header  string
	Content string
}

func GetSubSections(str string) []Group {
	reg := regexp.MustCompile(`(?m)^## (.+)\n\n((?:.+\n)+)`)
	regGroups := reg.FindAllStringSubmatch(str, -1)
	var groups []Group
	for _, g := range regGroups {
		groups = append(groups, Group{g[1], g[2]})
	}
	return groups
}

func GetBaseSection(str string) Group {
	reg := regexp.MustCompile(`(?m)^# (.+)\n\n((?:.*\n)+?)\n##`)
	a := reg.FindStringSubmatch(str)
	return Group{a[1], a[2]}
}
