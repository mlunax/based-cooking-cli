package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/logrusorgru/aurora/v3"
	"github.com/urfave/cli/v2"
)

type Recipe struct {
	Name        string
	BaseSection string
	Sections    []Group
}

func cliMagik(c *cli.Context) error {
	au := aurora.NewAurora(c.Bool("color"))
	doMagik(au, c.Args().Get(0), c)
	return nil
}

func doMagik(au aurora.Aurora, name string, c *cli.Context) {
	client := http.Client{}
	URL := fmt.Sprintf("https://raw.githubusercontent.com/LukeSmithxyz/based.cooking/master/src/%s.md", name)
	resp, err := client.Get(URL)
	if err != nil {
		panic(err)
	}
	var buffer []byte
	buffer, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	str := string(buffer)
	baseSection := GetBaseSection(str)
	sections := GetSubSections(str)
	recipe := Recipe{
		Name:        baseSection.Header,
		BaseSection: baseSection.Content,
		Sections:    sections,
	}
	if c.Bool("full") {
		RawContentGenerator(au, recipe)
	} else {
		BaseContentGenerator(au, recipe)
	}
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{Name: "color", Value: false},
		&cli.BoolFlag{Name: "full", Value: false},
	}
	app := &cli.App{
		Name:   "test",
		Usage:  "Get recipes from cooking.based",
		Action: cliMagik,
		Flags:  flags,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
