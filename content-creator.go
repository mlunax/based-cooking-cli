package main

import (
	"fmt"

	"github.com/logrusorgru/aurora/v3"
)

// func ParseFlags(flags)

func BaseContentGenerator(au aurora.Aurora, recipe Recipe) {
	recipeName := au.Green(recipe.Name).Bold()
	fmt.Print(recipeName, "\n", recipe.BaseSection)
}

func RawContentGenerator(au aurora.Aurora, recipe Recipe) {
	recipeName := au.Green(recipe.Name).Bold()
	baseSection := recipe.BaseSection
	subSections := ""
	for _, s := range recipe.Sections {
		subSections += fmt.Sprint(s.Header, "\n", s.Content, "\n")
	}
	fmt.Print(recipeName, "\n", baseSection, "\n", subSections)
}
